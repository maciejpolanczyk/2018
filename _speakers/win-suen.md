---
name: Win Suen
talks:
- 'Graph Analysis with Spark: Mapping >15 million Common Crawl websites (and staying
  sane through it all)'
---

Hello! My name is Win. Currently, I work as a data scientist at Appnexus. I fight online advertising fraud with science, learning tons along the way. I love to hack and poke into steaming piles of data. I encounter and beat big data into submission every day. Many of our logs exceed terabytes of data daily, so having a responsive, pleasure-to-use, general purpose computing tool like Spark extends the life expectancy of the data scientist. 

In my spare time, I like to hike, experiment with new recipes, read, and paint.

This talk relates to ongoing work to mine the Amazon Common Crawl. I learned a great deal about optimizations in representing and computing graphs, which will be of interest to even non-data scientists.