---
name: Collin Stedman
talks:
- 'MFA the Right Way: One-Time Passwords with PyOTP'
---

I am the CTO of Predata, a (growing!) predictive analytics startup based in SoHo, NYC. After jumping on the Node.js bandwagon in 2013, I finally saw the light and made the switch to Django/Python. When I'm not writing DRF code with gusto, I'm probably working on AWS DevOps of some sort.

My nerdy life goal is to solve 200+ problems on Project Euler. My less nerdy life goal is to learn to speak passable Mandarin.