---
abstract: The asyncio library in Python 3 is an amazing addition, but understanding
  how it works and why it's so efficient can be challenging. This talk covers the
  basics of asyncio and takes a deep dive on what it was like to convert the InterPlanetary
  File System Python 2 library to async enabled Python 3.
level: Intermediate
speakers:
- Brian Muller
title: I've Been Awaiting for an URL Like You
type: talk
---

The [InterPlanetary File System (IPFS)](https://ipfs.io/) is a peer-to-peer hypermedia protocol to make the web faster, safer, and more open.  It's one of the leading open projects in the decentralization space, and Python developers should be able to use libraries that support the API protocol.  The best way to interact with a network protocol like IPFS in Python is to utilize the asynchronous asyncio library that comes with Python 3.  This talk will cover the basics of how asyncio and IPFS work, and will then take deep dive on what it was like to convert the InterPlanetary File System Python 2 library to async enabled Python 3.  The talk will cover suggestions around when a conversion to asyncio may be useful, and when it may not be recommended.  General practices for code refactoring and Python 3 upgrades will be covered as well.