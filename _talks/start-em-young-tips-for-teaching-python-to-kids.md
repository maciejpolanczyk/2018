---
abstract: "How can you teach Python to kids with no coding experience? Using the example
  of a 1-week coding camp at the American Museum of Natural History, we\u2019ll share
  the power of combining coding lessons with scientific inquiry along with helpful
  tips and tricks to supercharge your education efforts."
level: All
speakers:
- Gabrielle Rabinowitz
title: "Start \u2018em young: Tips for teaching Python to kids"
type: talk
---

Whether you’re an experienced educator or you’re still wondering how to get started teaching others, this is a great opportunity to pick up some tips. In this talk, we’ll use a new Python and Earth Science curriculum piloted at the American Museum of Natural History as a model to share best practices for teaching Python to young students. We’ll discuss the value of collaborating with content experts, using online tools, and introducing coding concepts with games and hands-on activities. You’ll even get to walk through an example activity yourself!