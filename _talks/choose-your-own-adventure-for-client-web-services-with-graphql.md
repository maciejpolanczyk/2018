---
abstract: In the style of a modern, web-development choose your own adventure, we'll
  explore the benefits and trade offs of GraphQL and REST (including performance and
  scaling).  We'll build up an example schema using a choose your own adventure story
  as the model and leverage that schema in front end code.
level: Intermediate
speakers:
- David Anderson
title: Choose Your Own Adventure for Client Web Services with GraphQL
type: talk
---

In this talk, we discuss Facebook’s graphql standard, an a la carte way for front end clients to consume data from the backend, the python implementation of that standard (graphine), basic queries and mutations, and some advanced techniques. We’ll work up from shared principles with traditional REST web services to the new paradigm of data specific to each client request and how this makes back end and front end developers happier and more productive.

- About me, Prologue to the Adventure (5m)
- Chapter 1 - Architecture and Design (15m)
- Chapter 2 - Advanced Considerations (Authentication, Scaling, Performance, Rate Limiting, etc.) (10m)
- Chapter 3 - Building a Rich Client Web Application (10m)
- Epilogue + Questions
