---
abstract: Asyncio and gevent are powerful libraries for lightweight concurrency. The
  best ideas of both can be built straightforwardly from plain coroutines. Let's deep
  dive into coroutines and build our own concurrency library and poke around the internals
  of asyncio and gevent!
level: Intermediate
speakers:
- Vishal Prasad
title: From Coroutines to Concurrency
type: talk
---

Asyncio and gevent are powerful libraries for lightweight concurrency. Most of the core functionality of both can be built with great simplicity and clarity from plain coroutines, supplemented by elementary components of the Python 3.6 standard library (e.g. socket & queue). Building concurrency primitives from these raw materials is entertaining as an intellectual exercise, but also excellent in demystifying the aync tools which are all-too-often treated as magic.